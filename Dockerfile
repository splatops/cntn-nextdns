#
#
# ############################################### #
# #  splatops/cntn-nextdns/Dockerfile ########## #
# ############################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
# CONTAINER BASE
FROM python:3.8-slim

# VARIABLES
ENV \
    # NEXTDNS VARIABLES
    #
    # Path of cntn-nextdns root directory.
    ND_PATH=/opt/nextdns \
    # Name of cntn-nextdns main code.
    ND_BIN=nextdns.py \
    # Location of external file to override variables set here.
    ND_OVRD=./var/opt/variables.txt \
    # Time in seconds for running updates.  Sane default is 1800.
    # e.g. ND_SLEEP=1800
    # Leave blank if using override.
    ND_SLEEP= \
    # Placeholder for NextDNS programmatic update URL to be
    # statically built in the container if desired.
    # e.g. ND_LINKED_URL=https://link-ip.nextdns.io/a1111b/123456789abcdefg
    # Leave blank if using override.
    ND_LINKED_URL=

# SET PATH AFTER N_PATH
ENV PATH=${PATH}:${N_PATH}

# ENVIRONMENT BUILD
RUN \
echo "### Setup folder structure ###" && \
bash -c 'mkdir -p ${ND_PATH}/var/{log,opt}'

# CONTAINER STAGING
WORKDIR ${ND_PATH}
COPY nextdns.py .
COPY entrypoint.sh .
RUN chmod +x nextdns.py entrypoint.sh
ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "nextdns.py" ]