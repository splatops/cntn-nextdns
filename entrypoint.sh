#!/usr/bin/env bash
#
# ############################################### #
# #  splatops/cntn-nextdns/entrypoint.sh ####### #
# ############################################# #
#
# Brought to you by...
# 
# ::::::::::::'#######::'########:::'######::
# :'##::'##::'##.... ##: ##.... ##:'##... ##:
# :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
# '#########: ##:::: ##: ########::. ######::
# .. ## ##.:: ##:::: ##: ##.....::::..... ##:
# : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
# :..:::..:::. #######:: ##::::::::. ######::
# ::::::::::::.......:::..::::::::::......:::
#
##################################################
echo "##### entrypoint.sh start"
cd ${ND_PATH}

if [ -e ${ND_OVRD} ]
then
    echo "INFO: sourced override variables [entrypoint.sh]"
    source ${ND_OVRD}
    echo "INFO: ND_SLEEP (${ND_SLEEP})"
    echo "INFO: ND_LINKED_URL (${ND_LINKED_URL})"
else
    echo "INFO: no override variables found [entrypoint.sh]"
fi
echo "##### entrypoint.sh handoff"

cd ${ND_PATH}
${ND_BIN}

