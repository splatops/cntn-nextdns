#!/usr/bin/env python
import ipaddress
import json
import logging
import logging.handlers as handlers
import os
import sys
import time
import urllib.request
import urllib.error

LOG_NAME = 'var/log/nextdns.log'
URL_IP = 'https://api.ipify.org?format=json'

local_logger = logging.getLogger()
local_logger.setLevel(logging.DEBUG)
log_formatter = logging.Formatter('%(asctime)s-%(levelname)s | %(message)s', datefmt='%Y%m%d-%H:%M:%S')
#
file_handler = logging.handlers.RotatingFileHandler(LOG_NAME, maxBytes=500000, backupCount=1)
file_handler.setFormatter(log_formatter)
local_logger.addHandler(file_handler)
#
console_handler = logging.StreamHandler()
console_handler.setFormatter(log_formatter)
local_logger.addHandler(console_handler)


if os.environ.get('ND_SLEEP') == '':
    logging.warning("ND_SLEEP environment variable not set, exiting")
    sys.exit()
else:
    logging.info("ND_SLEEP environment variable set")
    SLEEP_SECS = int(os.environ.get('ND_SLEEP'))

if os.environ.get('ND_LINKED_URL') == '':
    logging.warning("ND_LINKED_URL environment variable not set, exiting")
    sys.exit()
else:
    logging.info("ND_LINKED_URL environment variable set")
    URL_ND = str(os.environ.get('ND_LINKED_URL'))


def external_ip_check(url):
    try:
        conn = urllib.request.urlopen(url)
    except urllib.error.HTTPError as e:
        # Generate log - HTTP exception
        logging.warning(f'HTTPError: {e.code} for {url}')
        return(0)
    except urllib.error.URLError as e:
        # Generate log - URL exception
        logging.warning(f'URLError: {e.reason} for {url}')
        return(0)
    except ValueError as e:
        # Generate log - URL value exception
        logging.warning(f'ValueError: {e}')
        sys.exit()
    else:
        # Service is functioning
        logging.info(f'URL open successful ({url})')
        ipaddr = json.load(conn)['ip']
        logging.info(f'External IP address returned ({ipaddr})')
        ipver = ipaddress.ip_address(ipaddr).version
        logging.info(f'IP address version ({ipver})')
        return(1)


def nextdns_update(url):
    try:
        conn = urllib.request.urlopen(url)
    except urllib.error.HTTPError as e:
        # Generate log - HTTP exception
        logging.warning(f'HTTPError: {e.code} for {url}')
    except urllib.error.URLError as e:
        # Generate log - URL exception
        logging.warning(f'URLError: {e.reason} for {url}')
    except ValueError as e:
        # Generate log - URL value exception
        logging.warning(f'ValueError: {e}, exiting')
        sys.exit()
    else:
        # Service is functioning
        logging.info(f'URL open successful ({url})')
        logging.info(f'Status code returned ({conn.status})')
        resp = conn.read()
        logging.info(f'HTTP response returned ({resp.decode("utf-8")})')


def updater():
    while True:
        logging.info("Starting external IP check")
        eip_status = external_ip_check(URL_IP)
        if eip_status == 1:
            logging.info("External IP check passed, updating NextDNS with IP address")
            nextdns_update(URL_ND)
        else:
            logging.info("External IP check failed, please check URL_IP or connectivity exception")
        logging.info(f'Sleep cycle ({SLEEP_SECS} secs)')
        time.sleep(SLEEP_SECS)


if __name__ == '__main__':
    updater()