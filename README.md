    # ############################################### #
    # #  splatops/cntn-nextdns/README ############## #
    # ############################################# #
    #
    # Brought to you by...
    # 
    # ::::::::::::'#######::'########:::'######::
    # :'##::'##::'##.... ##: ##.... ##:'##... ##:
    # :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
    # '#########: ##:::: ##: ########::. ######::
    # .. ## ##.:: ##:::: ##: ##.....::::..... ##:
    # : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
    # :..:::..:::. #######:: ##::::::::. ######::
    # ::::::::::::.......:::..::::::::::......:::
    #
    ##################################################
   

# Containerized NextDNS Programmatic URL Updater
SplatOps presents an opensource contribution to aid end users of NextDNS who are looking for a solution to keep "currently linked IP" up to date.

Link to: [NextDNS](https://www.nextdns.io)

Note: This project is not in any way affiliated with NextDNS.

## Getting Started
These instructions will walk you through:

* Building the NextDNS container image
* Running the NextDNS container image

Note: This guide assumes Docker is installed and generally understood by the end user.

### Building the NextDNS Container Image Locally
Clone this repository. Then...
* ```cd cntn-nextdns```
* ```docker build -t nextdns:latest .```
  
To validate Merlin is in your local container images...
* ```docker image ls | grep nextdns```

### Running the NextDNS Container Image
The NextDNS container image works by issuing an HTTP GET request to the programmatic URL which is uniquely provided per configuration.  To configured the NextDNS container image correctly first log into your NextDNS account, select the appropriate configuration and then copy the unique URL provided at the bottom of the "Linked IP" column.

Screenshot showing an example of the programmatic URL.  The URL required is at the very bottom of the example screenshot:

![NextDNS Programmatic URL Example](/doc/nextdns-00.png)

Once the URL has been copied it needs to be passed into the container image which can be done one of two ways.  The recommended approach is to provide environment variables in a variable override file.  An example file is located at `[cntn-nextdns-root]/var/opt/variables.txt.example`.  There are two variables located in the example file:

* ND_SLEEP - This variable controls the number of seconds between each HTTP GET request.  Please do not abuse this setting by creating calls that happen continuously!  A recommended setting of 1800 seconds (30 minutes) is likely more than frequent enough for most users as home Internet IPs, generally, do not change often.
* ND_LINKED_URL - This variable is the placeholder for the programmatic URL.  Paste the full link into this environment variable.

After this file has been updated save it as:

`[cntn-nextdns-root]/var/opt/variables.txt`

When the container starts and `nextdns.py` is started it will look for this file to be present.  To avoid rebuilding the container for each change to this file we will configure a bind mount for the `[cntn-nextdns-root]/var/` path.  This has the advantage of also exposing the native logging in the tool to be accessible from outside the container.  Logs are automatically rotated and configured to consume 1MB of log space across two log files.

The bind mount we'll use will look like this:

`-v /full/path/to/cntn-nextdns/var:/opt/nextdns/var`

The full commands from clone to run are as follows:
```
git clone {this repository}

cd ./cntn-nextdns/var/opt
cp variables.txt.example variables.txt
vi variables.txt

cd ../..

docker create \
--name cntn-nextdns \
-v `pwd`/var:/opt/nextdns/var \
cntn-nextdns:latest

docker start cntn-nextdns
```

To validate the container has started appropriately logs can be read directly from the container or locally in the host OS.
* Use Docker to Review Container Logs:
  * `docker logs --follow cntn-nextdns`
* Tail Logs Exposed Via Bind Mount:
  * `tail -f ./var/log/nextdns.log`
